// book.js

const Book = require('./book-model')

function registerBook(newBook) {
  Book.verifySchema(newBook)
  const found = Book.find(newBook)
  if (found)
    throw Error('This book already exist')

  const result = Book.save(newBook)

  return result
}

module.exports = {
  registerBook
}
