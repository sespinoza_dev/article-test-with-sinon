const { expect } = require('chai')
const sinon = require('sinon')
const bookmodule = require('../index')
const Book = require('../book-model')
const assert = require('assert')

describe('registerbook', () => {
  it('should verify book schema', async () => {
    const newBook = { title: 'the da vinci code' }
    const verifySchema = sinon.spy(Book, 'verifySchema')
    const saveStub = sinon.stub(Book, 'save')

    await bookmodule.registerBook(newBook)

    sinon.assert.calledOnce(verifySchema)
    assert(verifySchema.calledOnce)
    assert(verifySchema.withArgs(newBook).calledOnce)
    sinon.restore()
  })

  it('should throw and error if schema is not correct', async () => {
    const verifySchemaSpy = sinon.spy(Book, 'verifySchema')
    expect(() => bookmodule.registerBook({})).to.throw(/Title must be a string/)
    expect(verifySchemaSpy.threw('Error')).to.be.true
    expect(verifySchemaSpy.withArgs({}).calledOnce).to.be.true
    expect(verifySchemaSpy.exceptions[0].message).to.match(/Title must be a string/)
    sinon.restore()
  })

  it('should save the new book', async () => {
    const newBook = { title: 'the da vinci code' }
    const saveStub = sinon.stub(Book, 'save')
    bookmodule.registerBook(newBook)
    // both would work
    sinon.assert.calledOnce(saveStub)
    expect(saveStub.calledOnce).to.be.true
    sinon.restore()
  })

  it('should handle Book.save error', async () => {
    const newBook = { title: 'the da vinci code' }
    const saveStub = sinon
      .stub(Book, 'save')
      .throws(Error('could not save book'))
    expect(() => bookmodule.registerBook(newBook)).to.throw(/could not save book/)
    expect(saveStub.calledOnce).to.be.true
    sinon.restore()
  })

  it('should handle Book.save error', async () => {
    const newBook = { title: 'the da vinci code' }

    const saveStub = sinon
      .stub(Book, 'save')
      .withArgs({ title: 'Lord of the rings' })
      .returns('ok')
      .withArgs({ title: 'Twilight' })
      .returns('ugghh')

    expect(bookmodule.registerBook({ title: 'Lord of the rings'})).to.be.eq('ok')
    expect(bookmodule.registerBook({ title: 'Twilight'})).to.be.eq('ugghh')
    sinon.restore()
  })

  it('should handle Book.save error', async () => {
    const newBook = { title: 'the da vinci code' }
    const saveStub = sinon
      .stub(Book, 'save')
      .throws(Error('could not save book'))
    expect(() => bookmodule.registerBook(newBook)).to.throw(/could not save book/)
    expect(saveStub.calledOnce).to.be.true
    sinon.restore()
  })
})
