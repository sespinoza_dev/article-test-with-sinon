// book-model.js

const books = [
  { title: 'Origin', authorId: 1 },
  { title: 'Harry Potter', authorId: 2 }
]

const Book = {
  find: newBook => books.find(book => book.title == newBook.title),
  save: newBook => {
    console.log('saving book')
    books.push(newBook)
  },
  verifySchema: newBook => {
    if(typeof newBook.title != 'string')
      throw Error('Title must be a string')
  }
}

module.exports = Book

